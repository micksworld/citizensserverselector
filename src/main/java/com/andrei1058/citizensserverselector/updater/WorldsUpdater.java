package com.andrei1058.citizensserverselector.updater;

import com.andrei1058.citizensserverselector.utils.CounterType;
import com.andrei1058.citizensserverselector.utils.SpawnedNPC;
import org.bukkit.Bukkit;
import org.bukkit.World;

public class WorldsUpdater extends PlayerCounter {
    @Override
    public void updateHolograms() {
        for (SpawnedNPC s : SpawnedNPC.getNpcList()){
            if (s.getCounterType() == CounterType.WORLD){
                s.updatePlayerCounter();
            }
        }
    }

    @Override
    public int getCounter(String key){
        World w = Bukkit.getWorld(key);
        if (w == null) return 0;
        return w.getPlayers().size();
    }
}
