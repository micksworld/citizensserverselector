package com.andrei1058.citizensserverselector.listeners;

import com.andrei1058.citizensserverselector.utils.SpawnedNPC;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.NPCRemoveEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.HashMap;
import java.util.UUID;

public class CitizensListener implements Listener {

    private static HashMap<UUID, Long> antiSpam = new HashMap<>();

    @EventHandler
    public void removeNPC(NPCRemoveEvent e) {
        if (SpawnedNPC.getNpcById().get(e.getNPC().getId()) != null){
            SpawnedNPC.getNpcById().get(e.getNPC().getId()).delete(true);
        }
    }

    @EventHandler
    public void onNPCInteract(PlayerInteractEntityEvent e) {
        if (!e.getRightClicked().hasMetadata("NPC")) return;
        net.citizensnpcs.api.npc.NPC npc = CitizensAPI.getNPCRegistry().getNPC(e.getRightClicked());
        if (npc == null) return;
        SpawnedNPC sNPC = SpawnedNPC.getNpcById().get(npc.getId());
        if (sNPC == null) return;
        if (sNPC.getCommand().isEmpty()) return;
        e.setCancelled(true);
        if (isSpam(e.getPlayer())) return;
        Bukkit.dispatchCommand(e.getPlayer(), sNPC.getCommand());
        updateSpam(e.getPlayer());
    }

    /** Check if player can interact */
    private static boolean isSpam(Player p){
        if (antiSpam.containsKey(p.getUniqueId())){
            return antiSpam.get(p.getUniqueId())+5000 > System.currentTimeMillis();
        }
        return false;
    }

    /** Add in anti spam check*/
    @SuppressWarnings("WeakerAccess")
    public static void updateSpam(Player p){
        if (antiSpam.containsKey(p.getUniqueId())){
            antiSpam.replace(p.getUniqueId(), System.currentTimeMillis());
            return;
        }
        antiSpam.put(p.getUniqueId(), System.currentTimeMillis());
    }
}
