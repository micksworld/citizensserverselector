package com.andrei1058.citizensserverselector.listeners;

import com.andrei1058.citizensserverselector.Main;
import com.andrei1058.citizensserverselector.utils.CounterType;
import com.andrei1058.citizensserverselector.utils.SpawnedNPC;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class BungeeListener implements PluginMessageListener {

    private static boolean updateLater = false;

    @SuppressWarnings({"UnstableApiUsage", "NullableProblems"})
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput(bytes);
        String subChannel = in.readUTF();
        if (subChannel.equals("GetServers")) {
            String[] serverList = in.readUTF().split(", ");
            for (String server : serverList) {
                Main.getServersUpdater().addServer(server);
            }
            Main.getServersUpdater().setInitialized(true);
        } else if (subChannel.equals("PlayerCount")) {
            String server = in.readUTF();
            int count = in.readInt();
            Main.getServersUpdater().update(server, count);

            if (!updateLater) {
                updateLater = true;
                Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
                    updateLater = false;
                    for (SpawnedNPC s : SpawnedNPC.getNpcList()) {
                        if (s.getCounterType() == CounterType.SERVER) {
                            s.updatePlayerCounter();
                        }
                    }
                }, 20L);
            }
        }
    }
}
