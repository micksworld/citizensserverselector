package com.andrei1058.citizensserverselector.commands.main;

import com.andrei1058.citizensserverselector.commands.ParentCommand;
import com.andrei1058.citizensserverselector.commands.Permission;
import com.andrei1058.citizensserverselector.commands.SubCommand;
import com.andrei1058.citizensserverselector.utils.CitizensUtils;
import com.andrei1058.citizensserverselector.utils.SpawnedNPC;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.entity.Player;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SetCounterSubCmd extends SubCommand {
    /**
     * Create a sub-command
     * Make sure you return true or it will say command not found
     *
     * @param p    parent command
     * @param name sub-command name
     */
    @SuppressWarnings("WeakerAccess")
    public SetCounterSubCmd(ParentCommand p, String name) {
        super(p, name);
        setPriority(6);

        setDisplayInfo(MainCommand.createTC("§9" + MainCommand.getDot() + " §7/" + p.getName() + " " + getSubCommandName() + " §f- Where to get the players amount from.",
                "/" + p.getName() + " " + getSubCommandName() + " ",
                "§fServers or worlds where to get the \n§fplayers amount from.\n§fUse + for multiple servers or worlds.\n\n§cExample: §fbw1+bw2+bw3+bw4", ClickEvent.Action.SUGGEST_COMMAND));
        showInList(true);
    }

    @Override
    public boolean execute(String[] args, Player p) {

        if (!p.hasPermission(Permission.ALL_COMMANDS))return false;

        if (args.length < 1) {
            p.spigot().sendMessage(MainCommand.createTC("§9Usage: §7/css " + getSubCommandName() + " <name>",
                    "/css " + getSubCommandName() + " ", "§fServers or worlds where to get the \n§fplayers amount from.\n" +
                            "§fUse + for multiple servers or worlds.\n\n§cExample: §fbw1+bw2+bw3+bw4", ClickEvent.Action.SUGGEST_COMMAND));
            return true;
        }

        NPC npc = CitizensUtils.getTarget(p);
        if (npc == null) {
            p.sendMessage("§9Error: §7You must look at a NPC in order to use this command.");
            return true;
        }

        SpawnedNPC sNPC = SpawnedNPC.getNpcById().get(npc.getId());
        if (sNPC == null) {
            p.sendMessage("§9Error: §7The target NPC is not managed by this plugin.");
            return true;
        }

        StringBuilder target = new StringBuilder();
        for (String arg : args) {
            target.append(arg).append(" ");
        }
        target = new StringBuilder(target.substring(0, target.length() - 1));

        Pattern pat = Pattern.compile("[^A-Za-z0-9+]");
        Matcher m = pat.matcher(target.toString());
        if (m.find()) {
            p.sendMessage("§9Error: §7You can use numbers and letters only.");
            return true;
        }

        sNPC.setPlayerCounter(target.toString());

        p.sendMessage("§9" + MainCommand.getDot() + " Target counter set to: " + target);
        return true;
    }
}
