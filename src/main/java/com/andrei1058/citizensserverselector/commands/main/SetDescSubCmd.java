package com.andrei1058.citizensserverselector.commands.main;

import com.andrei1058.citizensserverselector.commands.Permission;
import com.andrei1058.citizensserverselector.commands.SubCommand;
import com.andrei1058.citizensserverselector.commands.ParentCommand;
import com.andrei1058.citizensserverselector.utils.CitizensUtils;
import com.andrei1058.citizensserverselector.utils.SpawnedNPC;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class SetDescSubCmd extends SubCommand {
    /**
     * Create a sub-command
     * Make sure you return true or it will say command not found
     *
     * @param p parent command
     * @param name   sub-command name
     */
    @SuppressWarnings("WeakerAccess")
    public SetDescSubCmd(ParentCommand p, String name) {
        super(p, name);
        setPriority(3);

        setDisplayInfo(MainCommand.createTC("§9" + MainCommand.getDot() + " §7/" + p.getName() + " " + getSubCommandName() + " §f- Add or change the counter hologram.",
                "/" + p.getName() + " " + getSubCommandName() + " ",
                "§fAdd or change the second hologram \n§fcontaining the player counter.\n§fUse the §c{players} §f placeholder.", ClickEvent.Action.SUGGEST_COMMAND));
        showInList(true);
    }

    @Override
    public boolean execute(String[] args, Player p) {

        if (!p.hasPermission(Permission.ALL_COMMANDS))return false;

        if (args.length < 1) {
            p.spigot().sendMessage(MainCommand.createTC("§9Usage: §7/css " + getSubCommandName() + " <name>",
                    "/css " + getSubCommandName() + " ", "§fAdd or change the second hologram " +
                            "\n§fcontaining the player counter.\n§fUse the &c{player} §f placeholder.", ClickEvent.Action.SUGGEST_COMMAND));
            return true;
        }

        NPC npc = CitizensUtils.getTarget(p);
        if (npc == null) {
            p.sendMessage("§9Error: §7You must look at a NPC in order to use this command.");
            return true;
        }

        SpawnedNPC sNPC = SpawnedNPC.getNpcById().get(npc.getId());
        if (sNPC == null) {
            p.sendMessage("§9Error: §7The target NPC is not managed by this plugin.");
            return true;
        }

        StringBuilder seconfLine = new StringBuilder();
        for (String arg : args) {
            seconfLine.append(arg).append(" ");
        }
        seconfLine = new StringBuilder(seconfLine.substring(0, seconfLine.length() - 1));

        sNPC.setSecondLine(seconfLine.toString());

        p.sendMessage("§9NPC counter set to " + ChatColor.translateAlternateColorCodes('&', seconfLine.toString()) + "§7.");
        return true;
    }
}
