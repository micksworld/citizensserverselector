package com.andrei1058.citizensserverselector.commands.main;

import com.andrei1058.citizensserverselector.Main;
import com.andrei1058.citizensserverselector.commands.ParentCommand;
import com.andrei1058.citizensserverselector.commands.SubCommand;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class JoinServerSubCmd extends SubCommand {
    /**
     * Create a sub-command
     * Make sure you return true or it will say command not found
     *
     * @param p    parent command
     * @param name sub-command name
     */
    public JoinServerSubCmd(ParentCommand p, String name) {
        super(p, name);
        setPriority(9);

        setDisplayInfo(MainCommand.createTC("§9" + MainCommand.getDot() + " §7/" + p.getName() + " " + getSubCommandName() + " §f- Connect to a server.",
                "/" + p.getName() + " " + getSubCommandName() + " ",
                "§fConnect to a server.", ClickEvent.Action.SUGGEST_COMMAND));
        showInList(true);
    }

    @Override
    public boolean execute(String[] args, Player p) {

        if (args.length != 1) {
            p.spigot().sendMessage(MainCommand.createTC("§9Usage: §7/css " + getSubCommandName() + " <server>",
                    "/css " + getSubCommandName() + " ", "§fConnect to a server.", ClickEvent.Action.SUGGEST_COMMAND));
            return true;
        }

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("Connect");
            out.writeUTF(args[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        p.sendPluginMessage(Main.getInstance(), "BungeeCord", b.toByteArray());
        return true;
    }
}
