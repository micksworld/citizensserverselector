package com.andrei1058.citizensserverselector.commands.main;

import com.andrei1058.citizensserverselector.commands.SubCommand;
import com.andrei1058.citizensserverselector.Main;
import com.andrei1058.citizensserverselector.commands.ParentCommand;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainCommand extends BukkitCommand implements ParentCommand {

    /* SubCommands List */
    private static List<SubCommand> subCommandList = new ArrayList<>();
    /* MainCommand instance*/
    private static MainCommand instance;
    /* Dot char */
    @SuppressWarnings("WeakerAccess")
    public static char dot = 254;

    public MainCommand(String name, List<String> aliases, String description) {
        super(name);
        setAliases(aliases);
        setDescription(description);
        instance = this;
        new CreateSubCmd(this, "create");
        new RenameSubCmd(this, "rename");
        new SetDescSubCmd(this, "setDesc");
        new SetSkinSubCmd(this, "setSkin");
        new SetCounterSubCmd(this, "setCounter");
        new CounterTypeSubCmd(this, "setType");
        new SetCommandSubCmd(this, "setCmd");
        new RemoveSubCmd(this, "remove");
    }

    @Override
    public boolean hasSubCommand(SubCommand subCommand) {
        return subCommandList.contains(subCommand);
    }

    @Override
    public void addSubCommand(SubCommand subCommand) {
        subCommandList.add(subCommand);
    }

    @Override
    public void sendSubCommandsList(Player p) {
        for (int i = 0; i <= 20; i++) {
            for (SubCommand sb : getSubCommands()) {
                if (sb.getPriority() == i && sb.isShow()) {
                    p.spigot().sendMessage(sb.getDisplayInfo());
                }
            }
        }
    }

    @Override
    public List<SubCommand> getSubCommands() {
        return subCommandList;
    }

    /**
     * Get instance
     */
    public static MainCommand getInstance() {
        return instance;
    }

    /**
     * Get dot symbol
     */
    public static char getDot() {
        return dot;
    }

    /**
     * Create a text component
     */
    public static TextComponent createTC(String text, String actionText, String shot_text, ClickEvent.Action action) {
        TextComponent tx = new TextComponent(text);
        tx.setClickEvent(new ClickEvent(action, actionText));
        tx.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(shot_text).create()));
        return tx;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public boolean execute(CommandSender s, String st, String[] args) {
        if (!(s instanceof Player)) return false;

        Player p = (Player) s;

        if (args.length == 0) {
            p.sendMessage("");
            p.spigot().sendMessage(createTC("§9" + Main.getInstance().getDescription().getName() + " - Commands",
                    "https://gitlab.com/andrei1058/citizensserverselector/wikis/home",
                    "&9Version: §f" + Main.getInstance().getDescription().getVersion() + "\n§9Author: §f" + Main.getInstance().getDescription().getAuthors().get(0), ClickEvent.Action.OPEN_URL));
            p.sendMessage("");
            sendSubCommandsList(p);
            return true;
        }

        boolean found = false;

        for (SubCommand sb : getSubCommands()) {
            if (sb.getSubCommandName().equalsIgnoreCase(args[0])) {
                found = sb.execute(Arrays.copyOfRange(args, 1, args.length), p);
            }
        }

        if (!found){
            p.sendMessage("§9"+getDot()+" Error: §7Command not found or insufficient permissions.");
        }
        return true;
    }
}
