package com.andrei1058.citizensserverselector.commands.main;

import com.andrei1058.citizensserverselector.commands.ParentCommand;
import com.andrei1058.citizensserverselector.commands.Permission;
import com.andrei1058.citizensserverselector.commands.SubCommand;
import com.andrei1058.citizensserverselector.utils.CitizensUtils;
import com.andrei1058.citizensserverselector.utils.CounterType;
import com.andrei1058.citizensserverselector.utils.SpawnedNPC;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.entity.Player;

public class CounterTypeSubCmd extends SubCommand {
    /**
     * Create a sub-command
     * Make sure you return true or it will say command not found
     *
     * @param p    parent command
     * @param name sub-command name
     */
    @SuppressWarnings("WeakerAccess")
    public CounterTypeSubCmd(ParentCommand p, String name) {
        super(p, name);
        setPriority(7);

        setDisplayInfo(MainCommand.createTC("§9" + MainCommand.getDot() + " §7/" + p.getName() + " " + getSubCommandName() + " §f- Set the counter type.",
                "/" + p.getName() + " " + getSubCommandName() + " ",
                "§fSet this to §cworld §fif the plugin should take\n§fthe players count from a world.\n" +
                        "§fSet this to §cserver §fif the plugin should take\n§fthe players count from a server.", ClickEvent.Action.SUGGEST_COMMAND));
        showInList(true);
    }

    @Override
    public boolean execute(String[] args, Player p) {

        if (!p.hasPermission(Permission.ALL_COMMANDS)) return false;

        if (args.length != 1) {
            sendUsage(p);
            return true;
        }

        NPC npc = CitizensUtils.getTarget(p);
        if (npc == null) {
            p.sendMessage("§9Error: §7You must look at a NPC in order to use this command.");
            return true;
        }

        try {
            CounterType.valueOf(args[0].toUpperCase());
        } catch (Exception e) {
            sendUsage(p);
            return true;
        }

        SpawnedNPC sNPC = SpawnedNPC.getNpcById().get(npc.getId());
        if (sNPC == null) {
            p.sendMessage("§9Error: §7The target NPC is not managed by this plugin.");
            return true;
        }

        try {
            CounterType.valueOf(args[0].toUpperCase());
        } catch (Exception e) {
            sendUsage(p);
            return true;
        }

        sNPC.setCounterType(CounterType.valueOf(args[0].toUpperCase()));

        p.sendMessage("§9" + MainCommand.getDot() + " Counter type set to: " + args[0].toUpperCase());

        return true;
    }

    private void sendUsage(Player p) {
        p.spigot().sendMessage(MainCommand.createTC("§9Usage: §7/css " + getSubCommandName() + " <type>\n§9Available types: SERVER, WORLD",
                "/css " + getSubCommandName() + " ", " §f- Specify if the counter is for worlds or servers." +
                        "§fSet this to §cworld §fif the plugin should take\n§fthe players count from a world.\n" +
                        "§fSet this to §cserver §fif the plugin should take\n§fthe players count from a server.", ClickEvent.Action.SUGGEST_COMMAND));
    }
}
