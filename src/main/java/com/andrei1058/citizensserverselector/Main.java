package com.andrei1058.citizensserverselector;

import com.andrei1058.citizensserverselector.commands.main.JoinServerSubCmd;
import com.andrei1058.citizensserverselector.commands.main.MainCommand;
import com.andrei1058.citizensserverselector.listeners.PlayerJoin;
import com.andrei1058.citizensserverselector.listeners.WorldListener;
import com.andrei1058.citizensserverselector.pluginsupport.PAPI;
import com.andrei1058.citizensserverselector.runnable.RefreshTask;
import com.andrei1058.citizensserverselector.configuration.ConfigManager;
import com.andrei1058.citizensserverselector.listeners.BungeeListener;
import com.andrei1058.citizensserverselector.listeners.CitizensListener;
import com.andrei1058.citizensserverselector.updater.ServersUpdater;
import com.andrei1058.citizensserverselector.updater.WorldsUpdater;
import com.andrei1058.citizensserverselector.utils.CitizensUtils;
import com.andrei1058.spigotutils.SpigotUpdater;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;

public class Main extends JavaPlugin {

    private static Main instance;
    private static ConfigManager storage;
    private static ServersUpdater serversUpdater;
    private static WorldsUpdater worldsUpdater;
    private static PAPI.PAPISupport papiSupport;

    public static boolean updateAvailable = false;
    public static String newVersion = "";


    @Override
    public void onEnable() {
        instance = this;

        //Software compatibility
        try {
            this.getServer().spigot();
        } catch (Exception e) {
            this.getLogger().severe("I can't run on your server software!");
            this.setEnabled(false);
            return;
        }

        //Citizens support
        if (this.getServer().getPluginManager().getPlugin("Citizens") != null) {
            getLogger().info("Hook into Citizens support!");
            Bukkit.getPluginManager().registerEvents(new CitizensListener(), this);
        } else {
            this.getLogger().severe("Citizens plugin was not found!");
            this.setEnabled(false);
            return;
        }

        //Register cmd
        try{
            final Field commandMapField = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            commandMapField.setAccessible(true);
            CommandMap commandMap = (CommandMap) commandMapField.get(Bukkit.getServer());
            commandMap.register("css", new MainCommand("css", Collections.singletonList("citizensserverselector"), "BungeePlugin command"));
        } catch (NoSuchFieldException  | IllegalArgumentException | IllegalAccessException exception){
            exception.printStackTrace();
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }


        //Load config
        saveDefaultStorage();

        //Setup updates
        if (this.getServer().spigot().getConfig().getBoolean("settings.bungeecord")) {
            getInstance().getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
            getInstance().getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new BungeeListener());
            Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
            MainCommand.getInstance().addSubCommand(new JoinServerSubCmd(MainCommand.getInstance(), "join"));
        }
        serversUpdater = new ServersUpdater();
        worldsUpdater = new WorldsUpdater();
        new RefreshTask().runTaskTimerAsynchronously(this, 20L, 20L * 10);
        Bukkit.getPluginManager().registerEvents(new WorldListener(), this);

        //Load existing NPCs
        Bukkit.getScheduler().runTaskLater(this, CitizensUtils::loadExisting, 40L);

        //Load PAPI support
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            papiSupport = new PAPI.WithPAPI();
        } else {
            papiSupport = new PAPI.WithoutPAPI();
        }

        new MetricsLite(this);

        // Check updates
        new SpigotUpdater(this, 60898, true).checkUpdate();

    }

    /**
     * Create storage file
     */
    private void saveDefaultStorage() {
        storage = new ConfigManager("storage", "plugins/" + this.getDescription().getName());
        YamlConfiguration yml = storage.getYml();
        yml.options().header("Plugin by andrei1058\nThis is the place where your NPCs will be stored.");

        yml.options().copyDefaults(true);
        storage.save();
    }

    /**
     * Get plugin instance
     */
    public static Main getInstance() {
        return instance;
    }

    /**
     * Get storage file instance
     */
    public static ConfigManager getStorage() {
        return storage;
    }

    /**
     * Get worlds updater instance
     */
    public static WorldsUpdater getWorldsUpdater() {
        return worldsUpdater;
    }

    /**
     * Get servers updater instance
     */
    public static ServersUpdater getServersUpdater() {
        return serversUpdater;
    }

    public static PAPI.PAPISupport getPapiSupport() {
        return papiSupport;
    }
}
