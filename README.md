![CitizensServerSelector](.gitlab/header.png)
A Minecraft plugin for lobby servers/ worlds.

You can click on a citizens NPC and it will execute a command. It can display a player counter for one or multiple servers connected to the same bungeecord instance or one or multiple minecraft worlds on the same spigot server.

#### Maven
```xml
<repository>
  <id>citizens-server-selector</id>
  <url>https://gitlab.com/api/v4/projects/14374071/packages/maven</url>
</repository>

<dependency>
  <groupId>com.andrei1058.citizensserverselector</groupId>
  <artifactId>CitizensServerSelector</artifactId>
  <version>VERSION_HERE</version>
</dependency>
```